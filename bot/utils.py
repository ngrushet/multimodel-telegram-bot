from aiogram.utils.helper import Helper, HelperMode, ListItem


class States(Helper):
    mode = HelperMode.snake_case

    STATE_CONTACT_SHARED    = ListItem()
    AUTHORIZED              = ListItem()
    NOT_AUTHORIZED          = ListItem()