# http://t.me/multi_llm_model_bot
import keyboards as kb
import os
import logging

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher, FSMContext
from aiogram.utils import executor
from aiogram import Bot, Dispatcher, executor, types

from dotenv import load_dotenv
import json
  
f = open('./config.json')
authorized_contacts = json.load(f)['authorized_contacts']
f.close()

load_dotenv()

API_TOKEN = os.getenv("TELEGRAM_BOT_API_KEY")
print(API_TOKEN)
# Enable logging
logging.basicConfig(level=logging.INFO)

bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

@dp.message_handler(content_types=types.ContentType.CONTACT)
async def contacts(msg: types.Message):
    print(msg)
    if (msg.from_user.id != msg.contact.user_id):
        await msg.answer(f"Красавчик, ты опубликовал не свой контакт : {msg.contact.phone_number}", reply_markup=types.ReplyKeyboardRemove())
    elif (format_numbers(msg.contact.phone_number) not in map(lambda x: format_numbers(x), authorized_contacts)):
        await msg.answer(f"Нет, красавчик, тебя нет в списке: {msg.contact.phone_number}", reply_markup=types.ReplyKeyboardRemove())
    else:
        await msg.answer(f"Красавчик, ты есть в списке : {msg.contact.phone_number}", reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler()
async def start_command(message: types.Message):
    print("received")

    await message.reply(
        "Поделись контактом",
        reply_markup=kb.markup_request
    )

def format_numbers(phone_number: str) -> str:
    numbers = list(filter(str.isdigit, phone_number))[1:]
    phone = "7{}{}{}{}{}{}{}{}{}{}".format(*numbers)
    print(phone)
    return phone
if __name__ == '__main__':
    executor.start_polling(dp)